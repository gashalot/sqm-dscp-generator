# DSCP Tagging Helper Scripts
This repository has a simple Python-based tool to generate Linux
[iptabls](https://en.wikipedia.org/wiki/Iptables) rules to set (or reset)
[DSCP](https://en.wikipedia.org/wiki/Differentiated_services) tags on some traffic.

This tool was written to assist [OpenWRT](https://openwrt.org) users properly classify
traffic for use with
[Smart Queue Management (SQM)](https://openwrt.org/docs/guide-user/network/traffic-shaping/sqm).
Most end-user applications fail to properly tag their traffic, resulting in SQM classifying
all traffic as best effort. While in most cases this works properly, certain cases, such as
WiFi calling and many video conferencing applications may be drowned out by other network traffic.

The example [config.txt](config.txt) contains up-to-date definitions for most popular US-based
cellular and video conferencing tools, and is capable of generating an out-of-the-box configuration
that can be used as a user-specific firewall script.

## Usage
To run the script, first edit the `config.txt` file to add any additional rules you may require.

Once finished, you can invoke the script by calling `python3 build.py ./path/to/config.txt`.

The final script is produced to stdout, and can be easily redirected to a file.
