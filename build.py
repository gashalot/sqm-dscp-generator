import argparse
import configparser
import ipaddress
import random
import re
from enum import Enum
from typing import Tuple, Optional, Iterable, Generator, Union, List, Set
import datetime

argp = argparse.ArgumentParser()
argp.add_argument("rules", type=argparse.FileType("r"))
args = argp.parse_args()


class TrafficClass(Enum):
    """Represents a simplified traffic classification, loosely aligned with SQM's diffserv4 algorithm."""

    # the values of the enum determine the order rules are emitted in the final script
    BEST_EFFORT = (
        0  # best effort has the "lowest" priority, since it only strips tagging
    )
    BULK = 1  # this will be given the lowest possible priority
    VIDEO = 2
    VOICE = 3

    @property
    def dscp_tag(self) -> Union[str, int]:
        """Return the iptables DSCP class associated with the class."""
        return {"BULK": "cs1", "BEST_EFFORT": 0, "VIDEO": "cs2", "VOICE": "cs4",}[
            self.name
        ]


class Protocol(Enum):
    TCP = "tcp"
    UDP = "udp"
    ICMP = "icmp"


class IpSet:
    def __init__(
        self,
        networks: Optional[
            Iterable[Union[ipaddress.IPv4Network, ipaddress.IPv6Network]]
        ] = None,
        domains: List[str] = tuple(),
    ):
        self.networks = networks or []
        self.__set_name = f"dscp_{random.getrandbits(32):02x}"  # generates a random ipset name to avoid collisions
        self.networks_4 = [n for n in networks or [] if n.version == 4]
        self.has_ip4 = len(self.networks_4) > 0
        self.networks_6 = [n for n in networks or [] if n.version == 6]
        self.has_ip6 = len(self.networks_6) > 0
        self.domains = domains
        self.has_domains = len(domains) > 0

        self.name_ip4 = f"{self.__set_name}" if self.has_ip4 else None
        self.name_ip6 = f"{self.__set_name}_ip6" if self.has_ip6 else None
        self.name_dns = f"{self.__set_name}_dns" if self.has_domains else None
        self.name_dns6 = f"{self.__set_name}_dns6" if self.has_domains else None
        self.dnsmasq_targets = [self.name_dns, self.name_dns6]

    def __hash__(self):
        return self.__set_name.__hash__()

    @property
    def names(self) -> List[str]:
        return [
            n
            for n in [self.name_ip4, self.name_ip6, self.name_dns, self.name_dns6]
            if n is not None
        ]

    def to_iptables(self) -> List[str]:
        entries: List[str] = []

        if self.has_ip4:
            entries.append(f"ipset -q create {self.name_ip4} hash:net")
            entries.append(f"ipset -q flush {self.name_ip4}")

        if self.has_ip6:
            entries.append(f"ipset -q create {self.name_ip6} hash:net family inet6")
            entries.append(f"ipset -q flush {self.name_ip6}")

        if self.has_domains:
            entries.append(f"ipset -q create {self.name_dns} hash:ip")
            entries.append(f"ipset -q create {self.name_dns6} hash:ip family inet6")

        for n in self.networks_4:
            entries.append(f"ipset add {self.name_ip4} {n.with_prefixlen}")

        for n in self.networks_6:
            entries.append(f"ipset add {self.name_ip6} {n.with_prefixlen}")

        return entries


class QosRule:
    def __init__(
        self,
        qos_class: TrafficClass,
        note: Optional[str] = None,
        protocol: Optional[Protocol] = None,
        ports: Optional[Iterable[str]] = None,
        targets: Optional[IpSet] = None,
        domains: Optional[Iterable[str]] = None,
    ):
        self.qos_class = qos_class
        self.note = note
        self.protocol = protocol
        self.ports = ports or []
        self.targets = targets
        self.domains = domains or []

    def to_dnsmasq(self) -> Iterable[str]:
        if self.targets is not None and self.targets.has_domains:
            return [
                f"ipset=/{d}/{','.join(self.targets.dnsmasq_targets)}"
                for d in self.domains
            ]
        else:
            return []

    def to_iptables(self) -> Iterable[str]:
        # by default flush and reload all of our specific rules
        entries = []

        # always tag traffic bidirectionally
        targets = []
        if self.targets:
            if self.targets.has_ip4:
                targets.append(["iptables", self.targets.name_ip4])

            if self.targets.has_ip6:
                targets.append(["ip6tables", self.targets.name_ip6])

        if len(self.domains) > 0:
            targets.append(["iptables", self.targets.name_dns])
            targets.append(["ip6tables", self.targets.name_dns6])

        for dirn in ["dst", "src"]:
            for prefix, tag in targets:
                opts = []
                if self.protocol is not None:
                    opts.append(f"-p {self.protocol.value}")

                opts.extend(["-m", "set", "--match-set", f"{tag}", dirn])

                if len(self.ports) > 0:
                    opts.append(
                        f"-m multiport --destination-ports {','.join(self.ports)}"
                    )

                if self.note is not None:
                    opts.extend(["-m", "comment", "--comment", f"'{self.note}'"])

                if isinstance(self.qos_class.dscp_tag, int):
                    entries.append(
                        f"{prefix} -t mangle -A POSTROUTING {' '.join(opts)} -j DSCP --set-dscp {self.qos_class.dscp_tag}"
                    )
                else:
                    entries.append(
                        f"{prefix} -t mangle -A POSTROUTING {' '.join(opts)} -j DSCP --set-dscp-class {self.qos_class.dscp_tag}"
                    )

        return entries


RE_PORTS = re.compile(
    r"(BEST_EFFORT|BULK|VIDEO|VOICE)(?:/(TCP|UDP|ICMP)\s*=?\s*([\d:\-,]*))?"
)


def parse_ports(
    line: str,
) -> Generator[Tuple[TrafficClass, Protocol, Iterable[str]], None, None]:
    """Iterates over each unique traffic classification and port definition in a config section."""
    for match in RE_PORTS.finditer(line):
        tc = TrafficClass[match.group(1)]
        proto = Protocol[match.group(2)] if match.group(2) else None
        ports = (
            [p for p in match.group(3).replace("-", ":").split(",") if len(p) > 0]
            if match.group(3)
            else []
        )

        if proto == "ICMP":
            yield tc, proto, []
        else:
            yield tc, proto, ports


class Rule:
    def __init__(self, line):
        if line.find(";"):
            provider, ports = line.split(";")
            self.provider = provider.strip()
            self.targets = list(parse_ports(ports))
        else:
            self.provider = "Unknown"
            self.targets = list(parse_ports(line))


def parse_file(in_f) -> Tuple[Iterable[IpSet], Iterable[QosRule]]:
    sets: Set[IpSet] = set()
    rules: List[QosRule] = []

    cfg = configparser.ConfigParser(
        allow_no_value=True,
        inline_comment_prefixes=(";", "#"),
        empty_lines_in_values=False,
    )

    cfg.read_file(in_f)

    for rule in cfg.sections():
        c = Rule(rule)
        all_hosts = False
        targets_ip: List[Union[ipaddress.IPv4Network, ipaddress.IPv6Network]] = []
        targets_dns: List[str] = []

        for target, _ in cfg.items(rule):
            for v in target.split(" "):
                if len(v.strip()) == 0:
                    continue

                network = None
                try:
                    network = ipaddress.ip_network(v, strict=False)

                    if network.with_prefixlen in ["0.0.0.0/0", "::/0"]:
                        all_hosts = True
                        break
                except:
                    pass

                if network is not None:
                    targets_ip.append(network)
                else:
                    targets_dns.append(v)

        target = IpSet(networks=targets_ip, domains=targets_dns)
        sets.add(target)

        for tc, proto, ports in c.targets:
            if all_hosts:
                rules.append(QosRule(tc, note=c.provider, protocol=proto, ports=ports,))
            elif len(targets_ip) > 0:
                rules.append(
                    QosRule(
                        tc,
                        note=c.provider,
                        targets=target,
                        protocol=proto,
                        ports=ports,
                    )
                )

            if len(targets_dns) > 0:
                rules.append(
                    QosRule(
                        tc,
                        note=c.provider,
                        targets=target,
                        domains=targets_dns,
                        protocol=proto,
                        ports=ports,
                    )
                )

    return sets, rules


if __name__ == "__main__":
    # parse the configuration file, storing rules as we go
    sets, rules = parse_file(args.rules)

    # emit the final script contents to stdout
    dnsmasq = []
    final_rules = []

    for ips in sets:
        final_rules.extend(ips.to_iptables())

    for rule in sorted(rules, key=lambda r: r.qos_class.value):
        dnsmasq.extend(rule.to_dnsmasq())
        final_rules.extend(rule.to_iptables())

    print(
        f"###\n# Generated at {datetime.datetime.now()}\n# See https://gitlab.com/gashalot/sqm-dscp-generator for more details.\n\n"
    )

    if len(dnsmasq) > 0:
        print("### DNSMASQ IPSETs\n# Overwrite local dnsmasq with newer ipset data")
        print("cat << EOF > /etc/dnsmasq.conf")
        for dns in dnsmasq:
            print(dns)
        print("EOF\n\n")

    print(
        "### IPTABLES\n# Create rules, with rules stripping DSCP tags added first so more specific matches override"
    )
    for r in final_rules:
        print(r)

    print("### RELOAD DNSMASQ\n/etc/init.d/dnsmasq reload")
